import 'package:flutter/material.dart';

void main() {
  runApp(const MaterialApp(
    title: 'Flutter第二天',
    home: ContainerDemo(),
  ));
}

class ContainerDemo extends StatelessWidget {
  const ContainerDemo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: GestureDetector(
        onTap: (){
          print('被监听到点击事件');
        },
        onDoubleTap: (){
          print('监听到双击事件');
        },
        child: Container(
          width: 300,
          height: 400,
          decoration: BoxDecoration(
              color: Colors.lightBlue,
              border: Border.all(width: 8.0, color: Colors.yellow)),
          child: const Text('容器展示'),
        ),
      ),
    );
  }
}
