import 'package:flutter/material.dart';

///文本控件
void main() {
  runApp(const MaterialApp(
    title: 'Flutter第二天',
    home: TextDemo(),
  ));
}

class TextDemo extends StatelessWidget {
  const TextDemo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('文本控件'),
      ),
      body: Column(
        children: const [
          Text(
            '红色+黑色删除线+25号字',
            style: TextStyle(
                color: Colors.red, //字体的颜色
                decorationColor: Colors.black, //装饰的颜色(删除线的颜色)
                decoration: TextDecoration.lineThrough,
                fontSize: 25.0),
          ),
          Text(
            'serif橙色+下划线+24号字',
            style: TextStyle(
                color: Colors.orange,
                decoration: TextDecoration.underline,
                fontSize: 24.0),
          ),
          Text(
            'serif虚线上划线+23号+倾斜',
            style: TextStyle(
                decorationStyle: TextDecorationStyle.dashed,
                decoration: TextDecoration.overline,
                fontStyle: FontStyle.italic,
                fontSize: 23.0),
          ),
          Text(
            'serif字体',
            style: TextStyle(fontFamily: 'serif', fontSize: 26.0),
          ),
          Text(
            'monospace加粗字体',
            style: TextStyle(
                fontFamily: 'monospace',
                fontSize: 24,
                fontWeight: FontWeight.bold),
          ),
          Text(
            '25号字,2行跨度',
            style:
                TextStyle(color: Colors.lightBlue, fontSize: 25, height: 2.0),
          ),
          Text(
            '2个字母间隔monospace',
            style: TextStyle(fontSize: 24, letterSpacing: 5.0),
          )
        ],
      ),
    );
  }
}
