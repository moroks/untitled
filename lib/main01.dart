import 'package:flutter/material.dart';

void main() {
  runApp(const MaterialApp(
    title: "Flutter第一天",
    home: MyScaffold(),
  ));
}

//const 代表被修饰的这一个控件或者这一句话相当于常量,页面刷新时不会进行刷新,节省性能
class MyAppBar extends StatelessWidget {
  const MyAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      //相当于div,盒子
      height: 56.0,
      padding: const EdgeInsets.symmetric(
          horizontal:
              8.0), //在flutter中,所有padding或者margin需要4个属性时,比如上下左右,需要是EdgeInsets
      decoration: const BoxDecoration(
          color: Colors.blue), //盒子的装饰,可以给当前的Container设置颜色或者风格等等
      child: Row(
        //是一个横向布局
        children: const [
          //代表子类的复数形式,可以有多个子控件
          IconButton(onPressed: null, icon: Icon(Icons.menu)),
          //图标按钮,可以有一个图标和一个点击事件,Icon代表flutter中自带的图标
          Expanded(child: Text("实例标题")), //占用了剩余的所有空间,相当于LinearLayout中的权重
          //文本
          IconButton(onPressed: null, icon: Icon(Icons.search))
        ],
      ),
    );
  }
}

class MyScaffold extends StatelessWidget {
  const MyScaffold({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      //质感设计,是google推出的一套设计风格,有固定的颜色和模板
      child: Column(
        children: const [
          MyAppBar(),
          Expanded(
              child: Center(
            child: Text("hello,world"),
          ))
        ],
      ),
    );
  }
}
