import 'package:flutter/material.dart';

void main() {
  runApp(const MaterialApp(
    title: 'Flutter第二天',
    home: FrameLayoutDemo(),
  ));
}

class LayoutDemo extends StatelessWidget {
  const LayoutDemo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('水平或者垂直方向布局'),
      ),
      body: Column(
        children: [
          RaisedButton(
            onPressed: () {
              print('点击了红色按钮');
            },
            child: const Text('红色按钮'),
            color: Colors.red,
          ),
          Expanded(
              child: RaisedButton(
            onPressed: () {
              print('点击了黄色按钮');
            },
            color: Colors.yellow,
            child: const Text('黄色按钮'),
          )),
          Expanded(
              flex: 5, //相当于权重
              child: ElevatedButton(
                  onPressed: () {
                    print('点击了E按钮');
                  },
                  child: const Text('E按钮')))
        ],
      ),
    );
  }
}

class FrameLayoutDemo extends StatelessWidget {
  const FrameLayoutDemo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('层叠布局'),
      ),
      body: Center(
        child: Stack(
          children: [
            Image.network(
                'http://pic.baike.soso.com/p/20130828/20130828161137-1346445960.jpg'),
            const Positioned(
              child: Text('这是一个层叠布局的文字'),
              left: 35,
              right: 35,
              top: 45,
            )
          ],
        ),
      ),
    );
  }
}
