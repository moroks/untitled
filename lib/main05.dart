import 'package:flutter/material.dart';

void main() {
  runApp(const MaterialApp(
    title: 'Flutter第二天',
    home: ImageLocalDemo(),
  ));
}

class ImageUrlDemo extends StatelessWidget {
  const ImageUrlDemo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('从URL地址获取图片'),
      ),
      body: Center(
        child: Image.network(
          'http://pic.baike.soso.com/p/20130828/20130828161137-1346445960.jpg',
          scale: 5.0,//缩放属性
        ),
      ),
    );
  }
}

class ImageLocalDemo extends StatelessWidget {
  const ImageLocalDemo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('从本地获取图片'),
      ),
      body: Center(
        child: Image.asset('images/333.png'),
      ),
    );
  }
}
