import 'package:flutter/material.dart';

/// Material Design(质感设计) google推出的一套设计语言,为了统一手机,平板,电视,手表等设计规范和UI规范,同时也是为了相互之间的UI交互
void main() {
  runApp(const MaterialApp(
    title: 'Flutter第一天', //Flutter中不区分单引号和双引号,都代表一个String
    home: Home(),
  ));
}

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const IconButton(
          icon: Icon(Icons.menu),
          onPressed: null,
          tooltip: '导航菜单',
        ),
        title: const Text('实例标题'),
        actions: const [
          IconButton(
            onPressed: null,
            icon: Icon(Icons.search),
            tooltip: '搜索',
          )
        ],
      ),
      body: const Center(
        child: Text('hello,world!'),
      ),
      floatingActionButton: const FloatingActionButton(
        onPressed: null,
        child: Icon(Icons.add),
        tooltip: '增加',
      ),
    );
  }
}
